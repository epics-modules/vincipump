# ---
# This is a test startup script
require essioc
require vincipump

epicsEnvSet("IP_ADDRESS", "192.168.1.101:502")
epicsEnvSet(SYSTEM, "SE-SEE")
epicsEnvSet(DEVICE, "SE-VINP-001")
epicsEnvSet(PREFIX, "$(SYSTEM):$(DEVICE):")
epicsEnvSet(LOCATION, "E03.110; $(IP_ADDRESS)")
epicsEnvSet(IOCNAME, "$(SYSTEM):$(DEVICE)")

# E3 Common databases
iocshLoad("$(essioc_DIR)/common_config.iocsh")

iocshLoad("$(vincipump_DIR)/vincipump.iocsh")
iocInit()
# ...

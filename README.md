# vincipump

European Spallation Source ERIC Site-specific EPICS module: vincipump

Simple IOC for the Modbus TCP/IP conenction with Vinci High Pressure Pump. IOC prepared for 3 drives but in this case only Drive 1 in use.

created by: Tamas Kerenyi WP12 (tamas.kerenyi@esss.se)

Additonal information:
* [Documentation](https://confluence.esss.lu.se/display/IS/Vinci+HP+syringe+pump)
* [Release notes](RELEASE.md)
* [Requirements](https://gitlab.esss.lu.se/nice/staging-recipes/vincipump-recipe/-/blob/master/recipe/meta.yaml#L18)
* [Building and Testing](https://confluence.esss.lu.se/display/IS/1.+Development+locally+with+e3+and+Conda#id-1.Developmentlocallywithe3andConda-BuildanEPICSmoduleandtestitlocally)
